import { Modal } from "antd";
import React, { useEffect, useState } from "react";
import { BasePerson } from "../../../constants/types";
import { usePersonInformation } from "../../hooks/usePersonInformation";

interface UpdateUserInfoProps {
	onOpen: boolean,
	onClose: Function,
	onData: any,
	onSuccess: Function,
}

export default function UpdateUserInfo({ onOpen, onClose, onData, onSuccess }: UpdateUserInfoProps) {

	const [open, setOpen] = useState<boolean>(false);
	const [newData, setNewData] = useState<BasePerson>();

	useEffect(() => {
		setOpen(onOpen);
		setNewData(onData);
		console.log(onData);
	}, [onOpen, onData]);

	function handleOk() {
		onSuccess(newData);
		setOpen(false);
	}

	function handleCancel() {
		setOpen(false);
	}
	function handleChange(e: any) {
		const name: keyof BasePerson = e.target.name;
		const value: keyof BasePerson = e.target.value;
		setNewData(e => ({...e, [name]: value}));
	}

	return(
		<>
			<Modal title='Update User Info' open={open} onOk={handleOk} onCancel={handleCancel}>
				<input type='text' name='name' value={newData?.name} onChange={e => handleChange(e)} />
				<input type='text' name='gender' value={newData?.gender} onChange={e => handleChange(e)}/>
				<input type='text' name='phone' value={newData?.phone} onChange={e => handleChange(e)}/>
				<input type='text' name='birthday' value={newData?.birthday} onChange={e => handleChange(e)}/>
			</Modal>
		</>
	)
}